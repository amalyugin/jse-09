package ru.t1.malyugin.tm.api;

import ru.t1.malyugin.tm.model.Command;

public interface ICommandRepository {

    Command[] getCommands();

}