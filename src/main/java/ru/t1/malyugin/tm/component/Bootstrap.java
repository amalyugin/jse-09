package ru.t1.malyugin.tm.component;

import ru.t1.malyugin.tm.api.ICommandController;
import ru.t1.malyugin.tm.api.ICommandRepository;
import ru.t1.malyugin.tm.api.ICommandService;
import ru.t1.malyugin.tm.constant.ArgumentConst;
import ru.t1.malyugin.tm.constant.CommandConst;
import ru.t1.malyugin.tm.controller.CommandController;
import ru.t1.malyugin.tm.repository.CommandRepository;
import ru.t1.malyugin.tm.service.CommandService;
import ru.t1.malyugin.tm.util.TerminalUtil;

public final class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private void processCommands() {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        while (!Thread.currentThread().isInterrupted()) {
            System.out.print("ENTER COMMAND: ");
            final String command = TerminalUtil.nextLine();
            processCommand(command);
        }
    }

    private void processCommand(final String command) {
        switch (command) {
            case CommandConst.VERSION:
                commandController.showVersion();
                break;
            case CommandConst.ABOUT:
                commandController.showAbout();
                break;
            case CommandConst.HELP:
                commandController.showHelp();
                break;
            case CommandConst.INFO:
                commandController.showSystemInfo();
                break;
            case CommandConst.EXIT:
                exit();
                break;
            default:
                commandController.showErrorCommand();
        }
    }

    private void processArguments(final String[] args) {
        if (args == null || args.length < 1) return;
        processArgument(args[0]);
        exit();
    }

    private void processArgument(final String argument) {
        switch (argument) {
            case ArgumentConst.VERSION:
                commandController.showVersion();
                break;
            case ArgumentConst.ABOUT:
                commandController.showAbout();
                break;
            case ArgumentConst.HELP:
                commandController.showHelp();
                break;
            case ArgumentConst.INFO:
                commandController.showSystemInfo();
                break;
            default:
                commandController.showErrorArgument();
        }
    }

    private void exit() {
        System.exit(0);
    }

    public void run(final String... args) {
        processArguments(args);
        processCommands();
    }

}